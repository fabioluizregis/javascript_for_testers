//Loops (Laços de repetição)

//for
// for (var a=0; a<10; a++) {
//     console.log(`Repetindo porque ${a} é menor que 10`)
// }

//while
// var i = 0;

// while (i <= 10) {
//     console.log(`Repetindo porque ${i} é menor que 10`)
//     i++;
// }

var avengers = ['Ironman', 'Spiderman', 'Thor', 'Capitain America', 'Black Panter', 'Black Widow']
avengers.forEach(function(value, key){
    console.log(`${value} na posição ${key}`)
})