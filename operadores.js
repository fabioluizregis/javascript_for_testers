//Operadores
// 2+2 = 4
// console.log(2+2);
// console.log(2-2);
// console.log(2*2);
// console.log(2/2);

//Comparação
// console.log(2 == 2); //true
// console.log(2 == 3); //false
// console.log(2 == "2"); //true - compara valor mas não o tipo
//não igual
// console.log(2 != 2); //false
// console.log(2 != 1); //true

//igual estrito
// console.log(2 === "2"); //false - compara valor E o tipo
// console.log(3 !== "3"); //true

//maior que
console.log(2 > 3);
console.log(2 > 1);

//menor que
//maior igual >=
//menor igual <=
console.log(2 >= 3);
console.log(2 >= 1);