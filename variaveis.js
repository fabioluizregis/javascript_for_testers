//variáveis - tipagem dinâmica
var number = 10;

console.log(number);
console.log(typeof number);

var name = "Fabio";
console.log(typeof name);

var n1 = 10;
var n2 = 20;
console.log(n1 + n2);

