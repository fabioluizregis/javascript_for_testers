// Arrays
// var avengers = new Array();

var avengers = ['Homem de Ferro', 'Capitão América', 'Thor']
console.log(avengers)
console.log(typeof avengers)

avengers.push('Hulk')
console.log(avengers)

avengers.push('Homem Aranha')
// avengers.pop(); //remove o último registro do array
// avengers.shift(); // remove o primeiro regristro do array

console.log(avengers)

var indice = avengers.indexOf('Homem Aranha')
console.log(indice)
avengers.splice(indice) // remove pelo índice posicional no array

avengers.push('Viúva Negra')
avengers.push('Gavião Arqueiro')

console.log(avengers)

var newAvengers = ['Homem Aranha', 'Capitã Marvel', 'Miss Marvel', 'Pantera Negra']

var result = avengers.concat(newAvengers)
console.log(result)